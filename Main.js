import React, { useState, useEffect } from "react";
import { View, Text, FlatList, StyleSheet, Image } from "react-native";
import fetchLaunches from "./api/fetchLaunches";

function Main() {
  const [data, setData] = useState(null);

  useEffect(() => {
    fetchLaunches(setData);
  }, []);

  const renderLaunches = ({ item }) => {
    return (
      <View style={styles.launchContainer}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerName}>{item.mission_name}</Text>
          {item.links.flickr_images && (
            <Image
              source={{ uri: item.links.flickr_images[0] }}
              style={styles.headerImage}
            />
          )}
        </View>
        {item.details && <Text style={styles.detailsText}>{item.details}</Text>}
      </View>
    );
  };

  if (data === null) {
    return <Text style={styles.status}>Loading</Text>;
  }

  return (
    <View>
      <Text style={styles.title}>Space-X Launches</Text>
      <Text style={{ textAlign: "center", marginBottom: 8 }}>
        {data.length} launches
      </Text>

      <FlatList
        renderItem={renderLaunches}
        keyExtractor={(item) => item.index}
        data={data}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  title: { textAlign: "center", fontSize: 22, marginVertical: 16 },
  launchContainer: {
    marginHorizontal: 24,
    marginBottom: 8,
    paddingBottom: 8,
    borderBottomColor: "#e0e0e0",
    borderBottomWidth: 1,
  },
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 8,
  },
  headerName: { fontSize: 18, maxWidth: 200 },
  headerImage: { width: 80, height: 80 },
  detailsText: {
    marginLeft: 4,
  },
  status: {
    fontSize: 18,
    textAlign: "center",
  },
  error: {
    color: "red",
  },
});

export default Main;
