const query = `query{
  launches {
    details
    id
    launch_success
    mission_name
    rocket {
      rocket_name
    }
     links {
      flickr_images
    }
  }
}`;

const fetchLaunches = async (setData) => {
  const response = await fetch("http://api.spacex.land/graphql/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: query,
    }),
  })
    .then((res) => res.json())
    .then((data) => setData(data.data.launches));
};

export default fetchLaunches;
