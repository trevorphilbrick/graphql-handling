import React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import { QueryClientProvider, QueryClient } from "react-query";
import Main from "./Main";

export default function App() {
  const queryClient = new QueryClient();

  return (
    <QueryClientProvider client={queryClient}>
      <SafeAreaView>
        <Main />
      </SafeAreaView>
    </QueryClientProvider>
  );
}
